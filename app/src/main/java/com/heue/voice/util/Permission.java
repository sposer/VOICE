package com.heue.voice.util;

import android.app.*;
import android.content.*;
import android.net.*;
import android.os.*;
import android.provider.*;
import android.view.View.*;
import android.util.*;

public class Permission
{
	public static void showPermission(final Context context) {
		simpleSetting(context);
	}

	public static void simpleSetting(Context context) {
		Intent intent = new Intent();
		try{
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			// 将用户引导到系统设置页面
			if (Build.VERSION.SDK_INT >= 9) {
				intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
				intent.setData(Uri.fromParts("package", context.getPackageName(), null));
			} else if (Build.VERSION.SDK_INT <= 8) {
				intent.setAction(Intent.ACTION_VIEW);
				intent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
				intent.putExtra("com.android.settings.ApplicationPkgName", context.getPackageName());
			} 
			context.startActivity(intent);
		} catch (Exception e) {//抛出异常就直接打开设置
			intent = new Intent(Settings.ACTION_SETTINGS);
			context.startActivity(intent);
		}
	}
}
