package com.heue.voice.util;

import android.content.Context;
import android.graphics.drawable.Icon;

import com.heue.voice.bean.TaskListBean;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonCfg {
    private final String path;
    private final File file;

    public JsonCfg(Context context) {
        path = context.getFilesDir().getAbsolutePath() + "/something.cfg";
        file = new File(path);
    }

    public JsonCfg(Context context, String name) {
        path = context.getFilesDir().getAbsolutePath() + name;
        file = new File(path);

    }


    public String readCfg() {
        String f = "";
        try {
            if (file.exists()) {
                f = FileReadWrite.read(path);
                JSONObject o = new JSONObject(f);
                f = o.getString("savePath");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return f;
    }

    public List<TaskListBean> readListCfg() {
        List<TaskListBean> list = new ArrayList<>();
        try {
            if (file.exists()) {
                String f = FileReadWrite.read(path);
                int length = f.length();
                if (length == 0) return list;
                JSONArray jsonArray = new JSONArray(f);
                length = jsonArray.length();
                JSONObject jsonObject;
                TaskListBean taskListBean;
                for (int i = 0; i < length; i++) {
                    jsonObject = jsonArray.getJSONObject(i);
                    //jsonObj格式：名称，信息（日期，大小）
                    taskListBean = new TaskListBean(jsonObject.getString("NAME"), jsonObject.getString("PATH"), jsonObject.getString("DATA"), 100);
                    list.add(taskListBean);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void writeCfg(String tag, String info) {
        try {
            JSONObject j = new JSONObject();
            j.put(tag, info);
            String l = j.toString();
            FileReadWrite.write(path, l);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void writeListCfg(TaskListBean bean) {
        try {
            String f = "";
            if (file.exists()) {
                f = FileReadWrite.read(path);
            }
            int length = f.length();
            JSONArray jsonArray;
            if (length != 0)
                jsonArray = new JSONArray(f);
            else
                jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("NAME", bean.getTaskName());
            jsonObject.put("PATH", bean.getFilePath());
            jsonObject.put("DATA", bean.getTaskAttrs());
            jsonArray.put(jsonObject);
            FileReadWrite.write(path, jsonArray.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public File getFile() {
        return file;
    }

}
