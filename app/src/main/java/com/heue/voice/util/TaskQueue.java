package com.heue.voice.util;

import android.os.Looper;

import java.util.*;
import java.util.Queue;
    import java.util.concurrent.LinkedBlockingDeque;

    /**
     * Java 单线程 Queue 队列
     * 顺序执行，先进先出
     */
    public class TaskQueue implements Runnable {
        private final Queue<Runnable> queue = new LinkedBlockingDeque<>();

        private volatile boolean hasBegin = false;
        private volatile boolean needStop = false;

        public TaskQueue(String name) {
            Thread thread = new Thread(this);
            thread.setName(name);
            thread.start();
        }

        /**
         * 添加任务，没有开启之前不notify通知
         * 开启后,添加任务并且notify通知
         */
        public void addTask(Runnable task) {
            if (needStop) {
                throw new IllegalThreadStateException("This thread already has been stopped");
            }

            queue.add(task);
            synchronized (queue) {
                if (hasBegin) {
                    queue.notify();//唤醒
                }
            }
        }

        /**
         * 当前可以开启任务
         */
        public void startLoopQueue() {
            if (needStop) {
                throw new IllegalThreadStateException("This thread already has been stopped");
            }

            synchronized (queue) {
                hasBegin = true;
                queue.notify();//唤醒
            }
        }

        public void stopLoopQueue() {
            needStop = true;
            synchronized (queue) {
                queue.notify();
            }
        }

        @Override
        public void run() {
            while (!needStop) {
                executeNextTask();
            }
        }

        /**
         * 监听获取任务
         */
        private synchronized void executeNextTask() {
            Runnable task;
            synchronized (queue) {
                //用于删除第一个头部元素，并且返回第一个头部元素，没有则返回null
                task = queue.poll();
            }
            if (task == null) {
                try {
                    synchronized (queue) {
                        queue.wait();// 继续等待
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                task.run();//去执行任务
            }
        }
        /*
    static int i = 0;

        public static void main(String[] args) {
            Runnable r = new Runnable() {

                @Override
                public void run() {
                    System.out.println(i);
                    i ++;
                }


            };
            TaskQueue tq = new TaskQueue("first");
            System.out.println("before task1");
            tq.addTask(r);
            System.out.println("before task2");
            tq.addTask(r);

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            tq.startLoopQueue();
            System.out.println("before task3");
            tq.addTask(r);
            System.out.println("end");
            tq.stopLoopQueue();

    }
    */
}
