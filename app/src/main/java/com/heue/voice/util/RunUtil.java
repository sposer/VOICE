package com.heue.voice.util;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

import com.heue.voice.R;

public class RunUtil {

    public static void isFirstRun(Activity context) {
        JsonCfg jsonCfg = new JsonCfg(context);
        if(!jsonCfg.getFile().exists()) {
            //是第一次
            //弹出提示窗
            Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.dialog_first);
            dialog.setTitle("第一次启动提示");
            TextView textView = dialog.findViewById(R.id.dialog_first_txt_tips);
            textView.setText("    你好用户，这是第一次启动的基本提示。\n\n应用的具体作用：\n");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                textView.append(stringColor("获取视频的声音，保存为AAC文件 ", context.getColor(R.color.colorAccent)));
            } else {
                textView.append(stringColor("获取视频的声音，保存为AAC文件 ", Color.parseColor("#F06292")));
            }
            textView.append("\n\n应用使用到的权限：\n");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                textView.append(stringColor("文件读写权限,该应用功能正是基于此开发 ", context.getColor(R.color.colorAccent)));
            } else {
                textView.append(stringColor("文件读写权限,该应用功能正是基于此开发 ", Color.parseColor("#F06292")));
            }
            textView.append("\n\n   接下来，应用会尝试申请一次，并将会在必要时检查权限，如果已经授予权限将不会有影响。");
            textView.append("\n\n如果使用中遇到问题，欢迎随时联系");
            dialog.show();
            JsonCfg cfg = new JsonCfg(context);
            cfg.writeCfg("savePath", context.getString(R.string.des_defaultDir));
            dialog.setOnDismissListener(dialog1 -> permissionCheck(context));
        }
    }

    public static boolean permissionCheck(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && activity.checkSelfPermission(
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            activity.requestPermissions(new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, 1);
            return false;
        }

        return true;
    }

    //结束程序
    public static void stopApp(Activity context) {
        context.finish();
        new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        }.start();
    }

    //部分字体颜色
    private static SpannableString stringColor(String string, int color) {
        SpannableString spanString = new SpannableString(string);
        ForegroundColorSpan span;
        //span = new ForegroundColorSpan(Color.parseColor(color));
        span = new ForegroundColorSpan(color);
        spanString.setSpan(span, 0, string.length() - 1, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        return  spanString;
    }
}
