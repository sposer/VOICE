package com.heue.voice.util;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {
    public static String getData() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");// HHmmss
        //获取当前日期
        Date date = new Date(System.currentTimeMillis());
        return simpleDateFormat.format(date);
    }
    
    public static String getTime() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HHmmss");// HHmmss
        //获取当前时间
        Date date = new Date(System.currentTimeMillis());
        return simpleDateFormat.format(date);
    }
    
    public static String formatData(long time) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");// HHmmss
        Date date = new Date(time);
        return simpleDateFormat.format(date);
    }
}
