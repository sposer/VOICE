package com.heue.voice.util;

import java.io.*;

public class FileReadWrite {
	public static String read(String pathname) {
        String line2 = "";
        try {
            //防止文件建立或读取失败，用catch捕捉错误并打印，也可以throw;
            //不关闭文件会导致资源的泄露，读写文件都同理
            //Java7的try-with-resources可以优雅关闭文件，异常时自动关闭文件；详细解读https://stackoverflow.com/a/12665271
            try (FileReader reader = new FileReader(pathname);
            BufferedReader br = new BufferedReader(reader)
            ) {
                String line;
                while ((line = br.readLine()) != null) {
                    line2 += line;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
            return line2;
	}

	public static boolean write(String dir, String info) {
        try {
            File file = new File(dir);
            FileWriter fw=new FileWriter(file);
            fw.write(info);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
            return true;
	}


}
