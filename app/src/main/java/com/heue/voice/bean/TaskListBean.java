package com.heue.voice.bean;

public class TaskListBean {
    private final String taskName;
    private String taskAttrs;
    private String filePath;
    private int taskProgress;
    public TaskListBean(String taskName, String taskDate, int taskProgress) {
        this.taskName = taskName;
        this.taskAttrs = taskDate;
        this.taskProgress = taskProgress;
    }

    public TaskListBean(String taskName, String filePath, String taskDate, int taskProgress) {
        this.taskName = taskName;
        this.filePath = filePath;
        this.taskAttrs = taskDate;
        this.taskProgress = taskProgress;
    }

    public void setTaskAttrs(String attrs) {
        this.taskAttrs = attrs;
    }

    public void setTaskProgress(int taskProgress) {
        this.taskProgress = taskProgress;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getTaskName() {
        return taskName;
    }

    public String getTaskAttrs() {
        return taskAttrs;
    }

    public int getTaskProgress() {
        return taskProgress;
    }

    public String getFilePath() {
        return this.filePath;
    }
}
