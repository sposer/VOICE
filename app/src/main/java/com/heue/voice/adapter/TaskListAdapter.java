package com.heue.voice.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.heue.voice.R;
import com.heue.voice.bean.TaskListBean;
import com.heue.voice.util.JsonCfg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TaskListAdapter extends BaseAdapter {
    private final LayoutInflater inflater;
    private final List<TaskListBean> list;

    public TaskListAdapter(Context context, List<TaskListBean> list) {
        this.inflater = LayoutInflater.from(context);
        if(list != null) this.list = list;
        else
            this.list = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public TaskListBean getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        // TODO Auto-generated method stub
        ViewHolder holder;
        if (convertView == null)
        {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_task, null);
            //holder.fileTypePic = convertView.findViewById(R.id.item_fileList_fileTypePic);
            holder.taskName = convertView.findViewById(R.id.item_taskList_fileName);
            holder.taskAttrs = convertView.findViewById(R.id.item_taskList_attrs);
            holder.progressBar = convertView.findViewById(R.id.dialog_progressBar);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }
        //holder.fileTypePic.setImageBitmap(list.get(position).getBitmap());
        holder.taskName.setText(list.get(position).getTaskName());
        holder.taskAttrs.setText(list.get(position).getTaskAttrs());
        if(list.get(position).getTaskProgress() != 0) {
            holder.progressBar.setMax(100);
            holder.progressBar.setProgress(list.get(position).getTaskProgress());
        } else  {
            holder.progressBar.setProgress(0);
        }
        if(map.get(position) == null) {
            map.put(position, holder.progressBar);
        }
        return convertView;
    }

    public static final class ViewHolder
    {
        //public ImageView fileTypePic;
        public TextView taskName;
        public TextView taskAttrs;
        public ProgressBar progressBar;
    }

    public void addItem(TaskListBean lb)
    {
        list.add(lb);
        notifyDataSetInvalidated();
    }

    public void remove(int position) {
        list.remove(position);
        map.remove(position);
        notifyDataSetChanged();
    }

    private final Map<Integer, ProgressBar> map = new HashMap<>();

    public ProgressBar getProgressBar(int position) {
        return map.get(position);
    }

    public void finishProgress(JsonCfg jsonCfg, String path, int position) {
        TaskListBean taskListBean = list.get(position);
        list.remove(taskListBean);
        taskListBean.setTaskProgress(100);
        taskListBean.setFilePath(path);
        list.add(position, taskListBean);
        notifyDataSetChanged();
        jsonCfg.writeListCfg(taskListBean);
    }
}
